# Láadan Zines

Content Zines for people to download, read, and distribute as desired.

## Ideas

1. Grammar lessons
2. Láadan entertainment...
    * Stories
    * Comics
3. Translate quotes?
4. News about Láadan or Suzette
5. Interviews with people who are interested in Láadan
